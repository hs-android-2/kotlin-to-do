package com.example.kotlintodoapp

data class TodoItem(val title: String, val status: Boolean)